## Connect
Console-based email client

## Wiki:

#### SYNOPSIS
- ./connect [OPTION]

#### DESCRIPTION    
-  -v
    verbose mode
    
-  -h
    show help
    
#### EXAMPLES
###### To launch the application you must set those environment variables as follow:
    $ export POPHOST= ...
    $ export POPUSER= ...
    $ export POPPWD= ...

###### Launch the application
    $ ./connect
    
###### Show the man page
    $ man -l connect.1
    
#### AUTHOR
- Cédric Bonhomme && Rémi Bertin

#### COPYRIGHT
- Copyright © 2013 Free Software Foundation, Inc.  License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent permitted by law.